/// Clones all the given variables into new variables. Intended for use with `move` closures.
///
/// # Examples
///
/// Compare this:
///
/// ```rust
/// # use std::sync::Arc;
/// # use std::sync::Mutex;
/// # use std::thread;
/// #
/// struct A {
///     x: Arc<Mutex<String>>,
/// }
///
/// # fn main() {
/// let i = Arc::new(Mutex::new(String::from("hello world")));
/// let a = A { x: Arc::new(Mutex::new(String::from("hello world"))) };
/// let o = Arc::new(Mutex::new(String::new()));
/// for _ in "hello world".chars() {
///     thread::spawn({
///         let i = i.clone();
///         let x = a.x.clone();
///         let o = o.clone();
///         move || {
///             let i_c = i.lock().unwrap().pop().unwrap();
///             {
///                 let x_c = x.lock().unwrap().pop().unwrap();
///                 assert_eq!(i_c, x_c);
///             }
///             o.lock().unwrap().push(i_c);
///         }
///     }).join();
/// }
/// # assert_eq!("", &i.lock().unwrap().clone());
/// # assert_eq!("", &a.x.lock().unwrap().clone());
/// # assert_eq!("dlrow olleh", &o.lock().unwrap().clone());
/// # }
/// ```
///
/// To this:
///
/// ```rust
/// #[macro_use]
/// extern crate clone_all;
/// # use std::sync::Arc;
/// # use std::sync::Mutex;
/// # use std::thread;
///
/// struct A {
///     x: Arc<Mutex<String>>,
/// }
///
/// # fn main() {
/// let i = Arc::new(Mutex::new(String::from("hello world")));
/// let a = A { x: Arc::new(Mutex::new(String::from("hello world"))) };
/// let o = Arc::new(Mutex::new(String::new()));
/// for _ in "hello world".chars() {
///     thread::spawn({
///         clone_all!(i, a.x, o);
///         move || {
///             let i_c = i.lock().unwrap().pop().unwrap();
///             {
///                 let x_c = x.lock().unwrap().pop().unwrap();
///                 assert_eq!(i_c, x_c);
///             }
///             o.lock().unwrap().push(i_c);
///         }
///     }).join();
/// }
/// # assert_eq!("", &i.lock().unwrap().clone());
/// # assert_eq!("", &a.x.lock().unwrap().clone());
/// # assert_eq!("dlrow olleh", &o.lock().unwrap().clone());
/// # }
/// ```
#[macro_export]
macro_rules! clone_all {
    ($i:ident) => {
        let $i = $i.clone();
    };
    ($i:ident, $($tt:tt)*) => {
        clone_all!($i);
        clone_all!($($tt)*);
    };
    ($this:ident . $i:ident) => {
        let $i = $this.$i.clone();
    };
    ($this:ident . $i:ident, $($tt:tt)*) => {
        clone_all!($this . $i);
        clone_all!($($tt)*);
    };
}

#[cfg(test)]
mod tests {
    struct A {
        x: String,
    }

    impl A {
        fn foo(&self) -> String {
            clone_all!(self.x);
            x
        }
    }

    #[test]
    fn test_clone_string() {
        let s: String = "hello world".into();
        {
            clone_all!(s);
            ::std::mem::drop(s);
        }
        ::std::mem::drop(s);
    }

    #[test]
    fn test_clone_multiple_strings() {
        let s1: String = "hello world".into();
        let s2: String = "goodbye".into();
        {
            clone_all!(s1, s2);
            ::std::mem::drop(s1);
            ::std::mem::drop(s2);
        }
        ::std::mem::drop(s1);
        ::std::mem::drop(s2);
    }

    #[test]
    fn test_clone_structure() {
        let a = A { x: "I am a struct".into() };
        let s1: String = "hello world".into();
        let s2: String = "goodbye".into();
        {
            clone_all!(a.x, s1, s2);
            let a_foo = a.foo();
            ::std::mem::drop(x);
            ::std::mem::drop(s1);
            ::std::mem::drop(s2);
            ::std::mem::drop(a_foo);
        }
        ::std::mem::drop(a);
        ::std::mem::drop(s1);
        ::std::mem::drop(s2);
    }
}
