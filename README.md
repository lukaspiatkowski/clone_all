# clone_all

`clone_all` provides a simple macro to clone all variables passed to it.

For example:

```rust
let x: String = "hello world".into();
{
    clone_all!(x);
    something_that_moves(x);
}
// we can still use x, because the x in the scope was cloned.
println!("{}", x);
```
